#! /bin/bash

BUILD_TYPE="release"

if [[ $# -ge 1 ]]; then
	BUILD_TYPE="$1"
	echo "Setting BUILD_TYPE to $1"
fi

while [[ "$(pwd)" != "/" ]] ; do
	if [ -d "build" ]; then
		cd "build"
		break
	fi
	cd ..
done

if [[ "$(pwd)" = "/" ]]; then
	echo "Error: No build-directory found" >&2
	exit 1
fi

if [[ -d "$BUILD_TYPE" ]]; then
	BUILD_DIR="$BUILD_TYPE"
else
	MATCHING_DIRS=$(ls -1 | grep "$BUILD_TYPE")
	MATCH_COUNT=$(echo "$MATCHING_DIRS" | wc -l)
	if [[ $MATCH_COUNT == 0 ]]; then
		echo "No matching build-directory found";
		exit 2
	elif [[ $MATCH_COUNT > 1 ]]; then
		echo "to many matching build-types";
		exit 2
	else
		BUILD_DIR="$MATCHING_DIRS"
	fi
fi

cd "$BUILD_DIR"

if [[ -f "build.ninja" ]]; then
	echo "Building with Ninja"
	cmake ../.. && ninja "${@:2}"
elif [[ -f "Makefile" ]]; then
	echo "Building using Make"
	cmake ../.. && make -j "$(nproc)" "${@:2}"
else
	echo "Error: Don't know how to build" >&2
	exit 3
fi


